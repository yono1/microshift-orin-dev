# NVIDIA Jetson OrinでKubernetes(MicroShift)を使用したアプリケーション開発体験キット

本リポジトリは、NVIDIA Jetson OrinへMicroShiftをインストールし、NVIDIA Riva APIをデプロイする方法をまとめます。

# GettingStarted
## STEP1. MicroShiftのインストール 

[MicroShiftのインストール方法](./doc/microshift.md)

### MicroShiftとは
MicroShiftとは、レッドハット社が開発を推進するオープンソースの軽量版OpenShiftです。MicroShiftのエンタープライズ版はRed Hat Device Edge(2023年3月現在 Developer Previewとしてリリース)が該当します。

エンタープライズ版のMicroShiftのホストOSは、Red Hat Enterprise Linux 8.7以降のみサポートされますが、オープンソースのMicroShiftは、v4.8のリリースまでバイナリ版がリリースされており、Debian系のOSでも実行可能です。

ここでは、MicroShift v4.8をNVIDIA Jetson OrinのOS Ubuntu 20.04へインストールする方法を以下に記載します。

>注意. MicroShiftは2023年1月のエンタープライズ版のリリースを契機にv4.8の次のリリースからv4.12以降の採番で開発が進められています。v4.12以降はソースコードとrpmパッケージのみリリースされているため、Debian系のOSで実行するにはソースコードからバイナリをビルドする必要があります。
>MicroShift v4.8は、Kubernetes v1.21/CRI-O v1.21とかなり古いKubernetesバージョンを使用しているため、本番環境での利用は避けてください。

## STEP2. NGCのアカウント作成
### アカウント登録
[NGC](https://ngc.nvidia.com/signin)のアカウント作成画面より、アカウント登録をしてください。

![picture 1](images/README/1682563578273.png)  

> 参考
> ABCI 2.0 User Guide - https://docs.abci.ai/ja/tips/ngc/

### NGCのAPI keyの生成
NGCにログインしたら、画面右上のメニューより[Setup]を選択し、[Generate API Key]を選択して、API Keyを作成します。

![picture 2](images/README/1682563898438.png)  

![picture 3](images/README/1682563942940.png)  

### NGC CLIのインストール
Orin上でNGC CLIをインストールします。

```
wget --content-disposition https://ngc.nvidia.com/downloads/ngccli_arm64.zip && \
unzip ngccli_arm64.zip && \
chmod u+x ngc-cli/ngcfind ngc-cli/ -type f -exec md5sum {} + | LC_ALL=C sort | md5sum -c ngc-cli.md5

echo "export PATH=\"\$PATH:$(pwd)/ngc-cli\"" >> ~/.bash_profile && source ~/.bash_profile
ngc config set
Enter API key [no-apikey]. Choices: [<VALID_APIKEY>, 'no-apikey'] ***　## 作成したNGCのAPI Keyを指定
Enter CLI output format type [ascii]. Choices: [ascii, csv, json]: json ## jsonがおすすめです
Enter org [no-org]. Choices: ['xxx']: xxx
Enter team [no-team]. Choices: ['no-team']: [Enter]
Enter ace [no-ace]. Choices: ['no-ace']: [Enter]
```

## STEP3. Riva APIサーバのデプロイ
NVIDIA Jetson AGX Orion上に[Riva API](https://docs.nvidia.com/deeplearning/riva/user-guide/docs/quick-start-guide.html)をデプロイします。

### NGCのkeyを取得
https://docs.nvidia.com/ngc/ngc-overview/index.html#registering-activating-ngc-account


### NVPモデルを変更
```
sudo nvpmodel -m 0
...
NVPM WARN: Reboot required for changing to this power mode: 0
NVPM WARN: DO YOU WANT TO REBOOT NOW? enter YES/yes to confirm: *YES*
```

### NGCレジストリからRiva APIのインストーラをダウンロード
```
ngc registry resource download-version nvidia/riva/riva_quickstart_arm64:2.13.0
cd riva_quickstart_arm64_v2.13.0
```

### Riva APIのインストーラのセットアップ
```
vi config.sh
# 変更箇所は以下です。

 15 # Enable or Disable Riva Services
 16 service_enabled_asr=true
 17 service_enabled_nlp=false
 18 service_enabled_tts=false
 19 service_enabled_nmt=false
...
 99     riva_model_loc="/data/model_repository"
```

### Riva APIのコンテナイメージのビルド

以下の通り、`riva_init.sh`のdockerバージョンを確認する箇所をコメントアウトします。
```
vi riva_init.sh
...
 64 #check_docker_version
 ...
```


```
mkdir `pwd`/model_repository
bash riva_init.sh
Logging into NGC docker registry if necessary...
Pulling required docker images if necessary...
Note: This may take some time, depending on the speed of your Internet connection.
> Pulling Riva Speech Server images.
  > Pulling nvcr.io/nvidia/riva/riva-speech:2.13.0-l4t-aarch64. This may take some time...
...
Riva initialization complete. Run ./riva_start.sh to launch services.
```

以下のコマンドを実行すると、コンテナイメージがビルドされていることがわかります。

```
podman images
REPOSITORY                        TAG                  IMAGE ID       CREATED       SIZE
nvcr.io/nvidia/riva/riva-speech          2.13.0-l4t-aarch64             4569877da686  6 weeks ago    12.4 GB
```

### Riva APIサーバのデプロイ

MicroShiftを用いてRiva APIコンテナをデプロイします。
Riva APIの起動にはAPIキーの登録が必要なため、以下の通り環境変数にキー情報を格納の上、コマンドを実行します。

```
export RIVA_API_KEY=$(echo xxx | base64)
cd microshift-orin-dev
oc create ns riva
oc project riva
cat manifest/riva-api/secret.yaml | envsubst | oc apply -f -
oc apply -f manifest/riva-api/sa.yaml 
oc apply -f manifest/riva-api/service.yaml 
oc apply -f manifest/riva-api/route.yaml 
oc apply -f manifest/riva-api/deployment.yaml 
oc get po
NAME                              READY   STATUS    RESTARTS   AGE
riva-api-server-76c86f847-2r8hk   1/1     Running   0          29s

oc logs riva-api-server-76c86f847-2r8hk
...
I1017 04:48:42.957298 20 feature-extractor.cc:415] TRITONBACKEND_ModelInitialize: conformer-en-US-asr-streaming-feature-extractor-streaming (version 1)
I1017 04:48:42.966259 20 model_lifecycle.cc:693] successfully loaded 'conformer-en-US-asr-streaming-endpointing-streaming' version 1
```

## STEP4. Riva Clientのデプロイ
Riva APIと接続するRiva Clientをデプロイします。
ここで使用するRiva Clientは、マイク入力を受けてRiva APIと連携するアプリケーションですので、事前にOrinにUSBマイクを接続してから進めてください。

```
git clone https://gitlab.com/yono1/microshift-orin-dev.git
podman login nvcr.io

# nvcr.ioへログイン
Username: $oauthtoken
Password: 
Login Succeeded!
```

コンテナイメージをビルドします。

```
cd src/riva-client
podman build -t riva-client:1.0 .
```

アプリケーションをデプロイします。

```
oc apply -f manifest/riva-client/deployment.yaml
oc get po 
NAME                              READY   STATUS    RESTARTS   AGE
riva-api-server-76c86f847-2r8hk   1/1     Running   0          2m1s
riva-client-7bf4fdfd68-gtmps      1/1     Running   0          37s
```

以下のコマンドを実行し、USBマイクに音声を入力すると、入力音声がテキストとして表示されることを確認します。

```
# 「テスト」と発話した例
oc get logs riva-client-7bf4fdfd68-gtmps -f
Action Partial> test, test, test
```

## STEP5. SunFounder PiCar-Vとの連携
同じLANに接続されるPiCar-V(w/ Raspberry pi 4B)とNVIDIA Jetson Orinへのネットワーク接続性を確認します。
なお、Picar-Vのセットアップは[こちら](https://gitlab.com/yono1/picar-meets-k8s)を参照してください。

```
ping picar.local
64 bytes from 192.168.2.104: icmp_seq=0 ttl=64 time=8.274 ms

ping orin.local
64 bytes from 192.168.2.100: icmp_seq=0 ttl=64 time=3.300 ms
```

マニフェストを修正して、Riva Clientを再度デプロイします。

```
vi manifest/riva-client/deployment.yaml
# 以下の行をPiCar-VのIPアドレスへ変更
        env:
        - name: PICAR_ENDPOINT 
          value: "<表示されたIPアドレス>:30000"
```

USBマイクに向かって、「Go」「Stop」「Left」「Right」「Up」「Down」と発話すると、PiCar-Vが声の指示通りに動作します。

```
oc logs riva-client-7bf4fdfd68-gtmps 
Action Partial> go : http://192.168.2.104:30000/run/?action=forward
Action Partial> stop : http://192.168.2.104:30000/run/?action=stop
...
```