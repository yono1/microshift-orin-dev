# MicroShiftのインストール

## 事前準備
```
/bin/bash
su -
. /etc/os-release
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key" | sudo apt-key add -
apt -y update
```

## 必要なパッケージ類をインストール
```
apt install -y curl jq runc podman iptables conntrack nvidia-container-runtime nvidia-container-toolkit

# Dockerは使用しないため停止しておきます。
systemctl stop docker.socket
systemctl stop docker
systemctl disable docker

# DockerでなくPodmanを使用
mv /usr/bin/docker /usr/bin/_docker
ln -s /usr/bin/podman /usr/bin/docker
```

> Podmanは、Docker互換のコンテナ管理ソフトです。Dockerと異なりデーモンレスで使用できます。

## CRI-O(コンテナランタイム)をインストール
```
curl https://raw.githubusercontent.com/cri-o/cri-o/release-1.21/scripts/get | bash
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.21.0/crictl-v1.21.0-linux-arm64.tar.gz
tar zxvf crictl-v1.21.0-linux-arm64.tar.gz
mv crictl /usr/local/bin
rm crictl-v1.21.0-linux-arm64.tar.gz
```

## CRI-OでGPUを使えるように設定
```
# デフォルトの設定を削除
rm /etc/crio/crio.conf.d/*
cat << EOF > /etc/crio/crio.conf.d/10-nvidia-runtime.conf
[crio.runtime]
default_runtime = "nvidia"

[crio.runtime.runtimes.nvidia]
runtime_path = "/usr/bin/nvidia-container-runtime"
EOF

cat << EOF > /etc/crio/crio.conf.d/01-crio-runc.conf
[crio.runtime.runtimes.runc]
runtime_path = "/usr/sbin/runc"
runtime_type = "oci"
runtime_root = "/run/runc"
EOF

cat << EOF > /etc/cni/net.d/10-crio-bridge.conf
{
    "cniVersion": "0.4.0",
    "name": "crio",
    "type": "bridge",
    "bridge": "cni0",
    "isGateway": true,
    "ipMasq": true,
    "hairpinMode": true,
    "ipam": {
        "type": "host-local",
        "routes": [
            { "dst": "0.0.0.0/0" }
        ],
        "ranges": [
            [{ "subnet": "10.42.0.0/24" }]
        ]
    }
}
EOF
```

## MicroShift(AIO: All-In-One)のインストール
```
curl -LO https://github.com/openshift/microshift/releases/download/nightly/microshift-linux-arm64
mv microshift-linux-arm64 /usr/local/bin/microshift; chmod 755 /usr/local/bin/microshift
```

## MicroShiftをsystemdで管理できるように設定
```
cat << EOF > /usr/lib/systemd/system/microshift.service
[Unit]
Description=MicroShift
After=crio.service

[Service]
WorkingDirectory=/usr/local/bin/
ExecStart=/usr/local/bin/microshift run
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
```

## CRI-OとMicroShiftを起動
```
systemctl enable crio --now
systemctl enable microshift.service --now
```

## MicroShiftのCLIツール(ocコマンド)をインストール
```
curl -LO https://mirror.openshift.com/pub/openshift-v4/arm64/clients/ocp/stable/openshift-client-linux.tar.gz
tar xvf openshift-client-linux.tar.gz
chmod +x oc
mv oc /usr/local/bin
rm openshift-client-linux.tar.gz 
export KUBECONFIG=/var/lib/microshift/resources/kubeadmin/kubeconfig
```

## インストール後の自動セットアップの状況を確認
```
oc get nodes
```

```
[実行結果]
NAME           STATUS   ROLES    AGE   VERSION
orin-desktop   Ready    <none>   42m   v1.21.0
```
> Nodeの状態が「Ready」であることを確認

```
oc get po -A
```

```
[実行結果]
NAMESPACE                       NAME                                  READY   STATUS    RESTARTS
kube-system                     kube-flannel-ds-gzzr8                 1/1     Running   1       
kubevirt-hostpath-provisioner   kubevirt-hostpath-provisioner-vzdrw   1/1     Running   1       
openshift-dns                   dns-default-9wmv5                     2/2     Running   2       
openshift-dns                   node-resolver-m6jnj                   1/1     Running   1       
openshift-ingress               router-default-85bcfdd948-n8thj       1/1     Running   1       
openshift-service-ca            service-ca-7764c85869-8b7j5           1/1     Running   2       
```

> 上記の結果となることを確認

## コンテナからGPUが利用可能かテスト
### テスト用のコンテナをデプロイ
```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: nvidia-l4t-base
spec:
  restartPolicy: OnFailure
  containers:
  - name: nvidia-l4t-base
    image: "quay.io/microshift/microshift-aio:l4t-test"
    command:
       - /usr/bin/deviceQuery
EOF
```

### コンテナのログを確認
```
oc logs nvidia-l4t-base
```

```
[実行結果]
/usr/bin/deviceQuery Starting...

 CUDA Device Query (Runtime API) version (CUDART static linking)

Detected 1 CUDA Capable device(s)

Device 0: "Orin"
  CUDA Driver Version / Runtime Version          11.4 / 10.2
  CUDA Capability Major/Minor version number:    8.7
  Total amount of global memory:                 30589 MBytes (32074567680 bytes)
MapSMtoCores for SM 8.7 is undefined.  Default to use 64 Cores/SM
MapSMtoCores for SM 8.7 is undefined.  Default to use 64 Cores/SM
  ( 8) Multiprocessors, ( 64) CUDA Cores/MP:     512 CUDA Cores
  GPU Max Clock rate:                            1300 MHz (1.30 GHz)
  Memory Clock rate:                             612 Mhz
  Memory Bus Width:                              128-bit
  L2 Cache Size:                                 4194304 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(131072), 2D=(131072, 65536), 3D=(16384, 16384, 16384)
  Maximum Layered 1D Texture Size, (num) layers  1D=(32768), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(32768, 32768), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  1536
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 2 copy engine(s)
  Run time limit on kernels:                     No
  Integrated GPU sharing Host Memory:            Yes
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Disabled
  Device supports Unified Addressing (UVA):      Yes
  Device supports Compute Preemption:            Yes
  Supports Cooperative Kernel Launch:            Yes
  Supports MultiDevice Co-op Kernel Launch:      Yes
  Device PCI Domain ID / Bus ID / location ID:   0 / 0 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >

deviceQuery, CUDA Driver = CUDART, CUDA Driver Version = 11.4, CUDA Runtime Version = 10.2, NumDevs = 1
Result = PASS
```
> 上記の通り、GPUの情報がコンテナから確認できることを確認

## mDNSの設定
MicroShiftは、mDNSの機能がビルトインされてるため、同一LAN上のホストからコンテナへ`.local`ドメインでアクセスできます。
ホスト側に以下の流れで`avahi`をインストールし、MicroShift上のコンテナの名前解決をできるようにしましょう。

### avahi-daemonのインストール
```
apt  install -y avahi-daemon
```

### avahi-daemonの起動
```
systemctl enable avahi-daemon --now
```

### nsswitchの設定変更
```
sed -i 's/files dns/files mdns4_minimal [NOTFOUND=return] dns/g' /etc/nsswitch.conf
```

これで、同じLAN上にある他のホストから、Orinに対して`orin.local`のドメインで名前解決できるようになります。

```
# 別のホストからping
vi /etc/nsswitch.conf
hosts:          files mdns4_minimal [NOTFOUND=return] dns

$ ping orin.loal
PING orin.local (100.64.1.55): 56 data bytes
64 bytes from 100.64.1.55: icmp_seq=0 ttl=64 time=8.822 ms
```